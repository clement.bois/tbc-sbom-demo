package com.orange.outdated;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OutdatedApplication {

	public static void main(String[] args) {
		SpringApplication.run(OutdatedApplication.class, args);
	}

}
